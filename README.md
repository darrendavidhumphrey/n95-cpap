#
A simple 3D printable adapter to make a face mask from a CPAP cushion

---

In response to the COVID-19 pandemic, numerous suggestions have been made on how to address the shortage of Personal Protective Equipiment (PPE), including face shields and masks.
This is my contribution. It is a simple and easy to print adapter that converts a Fisher and Paykel Simplus CPAP cushion into an effective, reusable face mask.

Testing is required to verify the efficaciousness of my design, but I believe it will prove to be a viable solution. It is my hope that it will surpass the N95 standard if properly fitted and assembled.

Creating the mask requires the following items:

- A Fisher and Paykel Simplus CPAP cushion 
- 4 feet of 1/2 inch elastic
- Fabric glue (Fabritac for example)
- or needle and thread to attack the elastic to the mask
- A 2" piece of duct tape or a small amount of silicone caulk
- A 2"x2" peice of HEPA filter material rated for better than 0.1 microns
- Approximately 6.5 meters of 3D printing filament. PETG is recommended.
- A typical 3D printer  (I use a Prusa i3 MK3S)

 ## Printing the adapter

Download the two STL files and load them into your favorite 3D printing slicer program. 
To simplify printing, I recommend orienting the 3D parts as shown here. 
(https://bitbucket.org/darrendavidhumphrey/n95-cpap/src/master/slicer.png)

For best results, enable support generation in your slicer.

I print at 0.3mm layer height and 20% infill on my Prusa i3 Mk3.  
A single adapter will print in about 90 minutes using the 0.30mm draft settings.

Once the adapter is printed, remove the support material.

## Assembling the adapter

Insert your filter material inside the filter box, then snap the two halves together.
The assembled filter unit will now snap into the CPAP cushion.

Cut the elastic into 4 one foot long pieces
Insert about an inch of elastic through each loop on the mask.
Fold the inserted elastic over to make a loop
Glue the elastic together using fabric glue, or if you prefer, securely sew the pieces together.

Finally, there is a series of ventilation holes on the nose of the CPAP cushion. These need to be sealed.
Use silicone caulk or a piece of duct tape to seal the holes.

The finished mask should look like this.
(https://bitbucket.org/darrendavidhumphrey/n95-cpap/src/master/FinishedMask.jpg)

## Wearing the mask
Place the mask over your nose and grab the top two elastic strings.
Pull the elastic behind your head until the mask is snug against your face.
Tie the elastic bands in a knot.
Repeat with the second pair of bands.

Try breating with the mask on. You should be drawing air in through the filter, not from the edges of the mask.
If you can feel air coming in on the sides, try pushing the mask tighter against your face to seal the leaks.
If needed, retie the elastic so the mask fits better

## Sterilizing the mask

If you printed with PETG, you can sterilize the mask by submerging it in a bath of 70C water.
If you used PLA, it may not be possible to safely sterilize the mask.

NOTE: More testing is needed to see how much sterilization time is necessary.

You'll need to remove and discard the filter element when you sterlize the mask.

Be careful not to let the water get over about 75C or the mask will start to melt and deform.
Keep the mask submerged in 70C water for at least half an hour.

## Filter material
To prevent viruses from passing through the filter you will need a filter rated for less than 0.1 microns.